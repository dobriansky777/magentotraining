<?php

declare(strict_types=1);

namespace Training\TestOM\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
    private $testPreference;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Training\TestOM\Model\PlayWithTest $testPreference
    ) {
        parent::__construct($context);
        $this->testPreference = $testPreference;
    }

    public function execute()
    {
        $this->testPreference->run();
        exit();
    }
}
