<?php

namespace Training\Rendering34\Plugin\Catalog\Block\Product\View;
use Magento\Catalog\Model\Product;

class Description extends \Magento\Catalog\Block\Product\View\Description {

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $registry);
    }

    public function beforeToHtml(
        \Magento\Catalog\Block\Product\View\Description $subject
    ) {

        $subject->getProduct()->setDescription('Test description');
        $subject->getProduct()->setSku('new sku');

    }

}
