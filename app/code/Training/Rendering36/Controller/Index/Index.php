<?php

declare(strict_types=1);

namespace Training\Rendering36\Controller\Index;

class Index implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    private $layoutFactory;
    private $resultRawFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Framework\View\LayoutFactory $layoutFactory

    ) {
        $this->layoutFactory = $layoutFactory;
        $this->resultRawFactory = $resultRawFactory;
    }

    public function execute()
    {
        $layout = $this->layoutFactory->create();
        $block = $layout->createBlock('Magento\Framework\View\Element\Template', 'somename')
            ->setTemplate('Training_Rendering36::test.phtml');

        $resultRaw = $this->resultRawFactory->create();
        return $resultRaw->setContents($block->toHtml());
    }
}
