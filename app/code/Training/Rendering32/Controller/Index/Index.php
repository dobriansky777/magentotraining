<?php

namespace Training\Rendering32\Controller\Index;

class Index implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    private $layoutFactory;
    private $resultRawFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
    )
    {
        $this->layoutFactory = $layoutFactory;
        $this->resultRawFactory = $resultRawFactory;
    }

    public function execute()
    {

        $layout = $this->layoutFactory->create();
        $block = $layout->createBlock('Training\Rendering32\Block\Test');
        $content = $block->toHtml();
        $resultRaw = $this->resultRawFactory->create();
        $resultRaw->setContents($content);
        $resultRaw->setHeader('Content-Type', 'text/html', true);
        return $resultRaw;
    }
}
