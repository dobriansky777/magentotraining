<?php

declare(strict_types=1);

namespace Training\Controllers\Controller\Index;

class Page /*extends \Magento\Framework\App\Action\Action*/ implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    private $resultPageFactory;

    public function __construct(
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}
