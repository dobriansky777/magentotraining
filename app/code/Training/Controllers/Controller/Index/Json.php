<?php

declare(strict_types=1);

namespace Training\Controllers\Controller\Index;

class Json /*extends \Magento\Framework\App\Action\Action*/ implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    private $resultJsonFactory;

    public function __construct(
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
    }

    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $resultJson->setData(['rtrt' => 3, 'ddd' => 'reere']);
        return $resultJson;
    }
}
