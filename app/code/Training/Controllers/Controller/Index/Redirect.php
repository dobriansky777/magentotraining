<?php

declare(strict_types=1);

namespace Training\Controllers\Controller\Index;

class Redirect /*extends \Magento\Framework\App\Action\Action*/ implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    private $resultRedirectFactory;

    public function __construct(
        \Magento\Framework\Controller\Result\RedirectFactory $resultRedirectFactory
    ) {
        $this->resultRedirectFactory = $resultRedirectFactory;
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect
            ->setPath('training_controllers/index/json', ['r' => 'fff', 'd' => '111', '_query' => ['d' => 'qqq', 'f' => 444]])
            ->setHttpResponseCode(301); // 302 by default
        return $resultRedirect;
    }
}
