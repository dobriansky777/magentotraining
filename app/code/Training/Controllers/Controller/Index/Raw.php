<?php

declare(strict_types=1);

namespace Training\Controllers\Controller\Index;

class Raw /*extends \Magento\Framework\App\Action\Action*/ implements \Magento\Framework\App\Action\HttpGetActionInterface
{
    private $resultRawFactory;

    public function __construct(
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
//        \Magento\Framework\Controller\ResultFactory $resultFactory
    ) {
        $this->resultRawFactory = $resultRawFactory;
    }

    public function execute()
    {
//        $resultRaw = $resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_RAW);

        $resultRaw = $this->resultRawFactory->create();
        $content = '<?xml version="1.0"?>
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:App/etc/routes.xsd">
    <router id="standard">
        <route id="training_controllers" frontName="testcontrollers">
            <module name="Training_Controllers" />
        </route>
    </router>
</config>
';
        $resultRaw->setContents($content);
        $resultRaw->setHeader('Content-Type', 'text/xml', true);
        return $resultRaw;
    }
}
